angular.module('parking',[])
.controller('filasParking',function($scope,$http){
    $scope.consultar = function(){
        if($scope.placa == undefined || $scope.placa == null){
            $scope.placa = 0;
        }
        // GET PUT POST DELETE
        $http.get("/suscripcion?placa="+$scope.placa).success(function(data){
            $scope.filas = [
                //{ id: 1, numcuenta: 1351651} ejemplo
                data
            ];
        });
    };
});

