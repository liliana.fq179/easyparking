/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parking.logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author lilia
 */
@Component("suscripcion") // en Springboot los modelos se convierten en componentes
public class Suscripcion {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //Atributos
    private int id_suscripciones;
    private String placa;
    private String plaza;
    private String dueno;
    private int id_dueno;
    private String tipo;
    private String fecha_inicio;
    private String fecha_final;

    //Contructor
    public Suscripcion(int id_suscripciones, String placa, String plaza, String dueno, int id_dueno, String tipo, String fecha_inicio, String fecha_final) {
        this.id_suscripciones = id_suscripciones;
        this.placa = placa;
        this.plaza = plaza;
        this.dueno = dueno;
        this.id_dueno = id_dueno;
        this.tipo = tipo;
        this.fecha_inicio = fecha_inicio;
        this.fecha_final = fecha_final;

    }

    public Suscripcion() {

    }

    //Metodos
    public int getId_suscripciones() {
        return id_suscripciones;
    }

    public void setId_suscripciones(int id_suscripciones) {
        this.id_suscripciones = id_suscripciones;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public int getId_dueno() {
        return id_dueno;
    }

    public void setId_dueno(int id_dueno) {
        this.id_dueno = id_dueno;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    //CRUD
    // Guardar
    public String guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        String sql = "INSERT INTO Suscripciones(id_suscripciones, placa, plaza, dueno, id_dueno, tipo, fecha_inicio, fecha_final) VALUES(?,?,?,?,?,?,?,?)";
        return sql;
    }

    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT id_suscripciones, placa, plaza, dueno, id_dueno, tipo, fecha_inicio, fecha_final FROM suscripciones WHERE placa = ?";
        List<Suscripcion> suscripciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Suscripcion(
                        rs.getInt("id_suscripciones"),
                        rs.getString("placa"),
                        rs.getString("plaza"),
                        rs.getString("dueno"),
                        rs.getInt("id_dueno"),
                        rs.getString("tipo"),
                        rs.getString("fecha_inicio"),
                        rs.getString("fecha_final")
                ), new Object[]{this.getPlaca()});
        if (suscripciones != null) {
            this.setId_suscripciones(suscripciones.get(0).getId_suscripciones());
            this.setPlaza(suscripciones.get(0).getPlaza());
            this.setDueno(suscripciones.get(0).getDueno());
            this.setId_dueno(suscripciones.get(0).getId_dueno());
            this.setTipo(suscripciones.get(0).getTipo());
            this.setFecha_inicio(suscripciones.get(0).getFecha_inicio());
            this.setFecha_final(suscripciones.get(0).getFecha_final());
// 
            return true;
        } else {
            return false;
        }

    }


    // Actualizar

    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE Suscripciones SET id_suscripciones = ?, plaza = ?, dueno = ?, id_dueno = ?, tipo = ?, fecha_inicio = ?, fecha_final = ? WHERE placa = ?";
        return sql;
    }

    // Borrar
    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM Suscripciones WHERE placa = ?";
        return sql;
       
    }

    // Sobre escribir el metodo ToString para ver los valor de los atributos de una clase
    @Override
        public String toString() {
        return "Suscripcion{" + "placa=" + placa + "id_suscripciones= " + id_suscripciones + "plaza= " + plaza + "dueno= " + dueno + "id_dueno= " + id_dueno + "tipo= " + tipo + "fecha_inicio= " + fecha_inicio + "fecha_final" + fecha_final + '}';
    }

}



